.. _api_functions:


Library information
===================

SPD_getLibVersion
-----------------

.. doxygenfunction:: SPD_getLibVersion
   :project: SPD - API guide




Connection Functions
====================

SPD_listDevices
---------------

.. doxygenfunction:: SPD_listDevices
   :project: SPD - API guide

SPD_openDevice
--------------

.. doxygenfunction:: SPD_openDevice
   :project: SPD - API guide

SPD_closeDevice
---------------

.. doxygenfunction:: SPD_closeDevice
   :project: SPD - API guide

SPD_stopActivity
----------------

.. doxygenfunction:: SPD_stopActivity
   :project: SPD - API guide





Save and Reset Settings
=======================

SPD_saveAllSettings
-------------------

.. doxygenfunction:: SPD_saveAllSettings
   :project: SPD - API guide

SPD_factorySettings
-------------------

.. doxygenfunction:: SPD_factorySettings
   :project: SPD - API guide





Device Information
==================

SPD_getSystemVersion
--------------------

.. doxygenfunction:: SPD_getSystemVersion
   :project: SPD - API guide

SPD_getSystemFeature
--------------------

.. doxygenfunction:: SPD_getSystemFeature
   :project: SPD - API guide





Recover Parameters Range
========================

SPD_getClockFreqRange
---------------------

.. doxygenfunction:: SPD_getClockFreqRange
   :project: SPD - API guide

SPD_getEfficiencyRange
----------------------

.. doxygenfunction:: SPD_getEfficiencyRange
   :project: SPD - API guide

SPD_getDelayRange
-----------------

.. doxygenfunction:: SPD_getDelayRange
   :project: SPD - API guide

SPD_getDeadTimeRange
--------------------

.. doxygenfunction:: SPD_getDeadTimeRange
   :project: SPD - API guide

SPD_getGateWidthRange
---------------------

.. doxygenfunction:: SPD_getGateWidthRange
   :project: SPD - API guide





Set and Get Parameters
======================

SPD_setEfficiency
-----------------

.. doxygenfunction:: SPD_setEfficiency
   :project: SPD - API guide

SPD_getEfficiency
-----------------

.. doxygenfunction:: SPD_getEfficiency
   :project: SPD - API guide

SPD_setGateWidth
----------------

.. doxygenfunction:: SPD_setGateWidth
   :project: SPD - API guide

SPD_getGateWidth
----------------

.. doxygenfunction:: SPD_getGateWidth
   :project: SPD - API guide

SPD_setDelay
------------

.. doxygenfunction:: SPD_setDelay
   :project: SPD - API guide

SPD_getDelay
------------

.. doxygenfunction:: SPD_getDelay
   :project: SPD - API guide

SPD_setDeadTime
---------------

.. doxygenfunction:: SPD_setDeadTime
   :project: SPD - API guide

SPD_getDeadTime
---------------

.. doxygenfunction:: SPD_getDeadTime
   :project: SPD - API guide

SPD_setClockingMode
-------------------

.. doxygenfunction:: SPD_setClockingMode
   :project: SPD - API guide

SPD_getClockingMode
-------------------

.. doxygenfunction:: SPD_getClockingMode
   :project: SPD - API guide

SPD_setClockFreq
----------------

.. doxygenfunction:: SPD_setClockFreq
   :project: SPD - API guide

SPD_getClockFreq
----------------

.. doxygenfunction:: SPD_getClockFreq
   :project: SPD - API guide

SPD_setIntegrationTime
----------------------

.. doxygenfunction:: SPD_setIntegrationTime
   :project: SPD - API guide

SPD_getIntegrationTime
----------------------

.. doxygenfunction:: SPD_getIntegrationTime
   :project: SPD - API guide

SPD_setModuleDetectionMode
--------------------------

.. doxygenfunction:: SPD_setModuleDetectionMode
   :project: SPD - API guide

SPD_getModuleDetectionMode
--------------------------

.. doxygenfunction:: SPD_getModuleDetectionMode
   :project: SPD - API guide

SPD_setOutputFormat
-------------------

.. doxygenfunction:: SPD_setOutputFormat
   :project: SPD - API guide

SPD_getOutputFormat
-------------------

.. doxygenfunction:: SPD_getOutputFormat
   :project: SPD - API guide

SPD_setIntegTimeAO
------------------

.. doxygenfunction:: SPD_setIntegTimeAO
   :project: SPD - API guide

SPD_getIntegTimeAO
------------------

.. doxygenfunction:: SPD_getIntegTimeAO
   :project: SPD - API guide

SPD_setAnalogOutGain
--------------------

.. doxygenfunction:: SPD_setAnalogOutGain
   :project: SPD - API guide

SPD_getAnalogOutGain
--------------------

.. doxygenfunction:: SPD_getAnalogOutGain
   :project: SPD - API guide

SPD_setDetOutDelay
------------------

.. doxygenfunction:: SPD_setDetOutDelay
   :project: SPD - API guide

SPD_getDetOutDelay
------------------

.. doxygenfunction:: SPD_getDetOutDelay
   :project: SPD - API guide





Monitoring Functions
====================

SPD_getCLKCountData
-------------------

.. doxygenfunction:: SPD_getCLKCountData
   :project: SPD - API guide

SPD_getClkCountModule
---------------------

.. doxygenfunction:: SPD_getClkCountModule
   :project: SPD - API guide

SPD_getSystemAlarms
-------------------

.. doxygenfunction:: SPD_getSystemAlarms
   :project: SPD - API guide