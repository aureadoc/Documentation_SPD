.. _api:

All Functions
=============

This section provides the prototypes and descriptions of all functions    
integrated into SPD library.

.. warning::

   More or less functions are available according to the device type.  
   The compatibility depends on the device part number recovered by the "SPD_getSystemVersion" function.                      
   Please see notes functions to check the compatibility with your device:    
   -> SPD version compatibility:  PN_SPD_x_Mx_xx_xx_xx   
   Refer to section :ref:`Code Examples`, to recover version in C++ or in Python.

.. toctree::
   :maxdepth: 2
   :glob:

   *