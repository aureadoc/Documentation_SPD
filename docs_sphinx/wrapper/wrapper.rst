.. _wrapper:

C++ Wrapper
===========

Wrapper Advantage
~~~~~~~~~~~~~~~~~

A C++ wrapper has been created for several reasons.
	- To make SPD functions easy to use.
	- To allow multiple SPD device control in the same application and at the same time.
	- To link Dynamic Library inside C++ code and not in the project configuration.

.. note::

	Except OpenDevices function, you do not need to specify iDev when using SPD wrapper function.

	For example function ``SPD_getClkCountModule(short iDev, short module, unsigned long *Clock,unsigned long *Count)`` can be replace by ``ObjectName.GetClkCountModule(short module, unsigned long *Clock,unsigned long *Count)``

C++ code
~~~~~~~~

Here is an example of how to use this wrapper to recover data from 2 SPD :

.. code-block:: c++

	#include <iostream>
	using namespace std;

	#include "SPD_wrapper.h"
	#include "SPD.h"

	// Select shared library compatible to current operating system
	#ifdef _WIN32
	#define DLL_PATH L"SPD.dll"
	#elif __unix
	#define DLL_PATH "SPD.so"
	#else
	#define DLL_PATH "SPD.dylib"
	#endif

	int main(int argc, const char* argv[]) {
		short iDev = 0;
		short ret;
		char* devicesList[10];
		short numberDevices;
		short ModuleNumber;
		short sValue;
		unsigned long CLK1 = 0, CLK2 = 0, Count1 = 0, Count2 = 0;

		// Instancie the device from wrapper
		SPD_wrapper SPD0(DLL_PATH);
		SPD_wrapper SPD1(DLL_PATH);

		// Loop to list devices until at least two are founded
		ret = SPD0.ListDevices(devicesList, &numberDevices);
		if (ret == 0) {
			if (numberDevices == 0 || numberDevices == 1) {
				cout << endl << "    Please connect AT device !" << endl << endl;
				do {
					delay(500);
					ret = SPD0.ListDevices(devicesList, &numberDevices);
				} while (numberDevices == 0 || numberDevices == 1);
			}
		}

		// Open communication with device 0
		printf(" -%u: %s\n", 0, devicesList[0]);
		SPD0.OpenDevice(0);
		printf("\n SPD %d-> Communication Open\n\n", 0);

		// Open communication with device 1
		printf(" -%u: %s\n", 1, devicesList[1]);
		SPD1.OpenDevice(1);
		printf("\n SPD %d-> Communication Open\n\n", 1);

		// Recover Clock and Photons count for both devices
		SPD0.GetClkCountModule(0, &CLK1, &Count1);
		printf("\n\nSPD 0 -> Clock: %7lu	Hz	Counts : %7lu", CLK1, Count1);
		SPD1.GetClkCountModule(0, &CLK2, &Count2);
		printf("\nSPD 1 -> Clock: %7lu	Hz	Counts : %7lu", CLK2, Count2);

		// Wait some time
		delay(2000);

		/*    CloseDevice function    */
		// Close initial device opened: MANDATORY AFTER EACH END OF SYSTEM COMMUNICATION.
		if (SPD0.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;
		if (SPD1.CloseDevice() == 0) cout << "   -> Communication closed" << endl;
		else cout << "  -> Failed to close communication" << endl;

		// Call class destructor
		SPD0.~SPD_wrapper();
		SPD1.~SPD_wrapper();

		return 0;
	}
