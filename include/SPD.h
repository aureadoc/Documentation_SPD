////////////////////////////////////////////////////////////////////////////// 
/// 
/// \mainpage			SPD Library
///
///--------------------------------------------------------------------------- 
/// \section det_sec Details
/// - FileName		:	SPD.h
/// - Dependencies	:   None
/// - Compiler		:   C++
/// - Company		:   Copyright (C) Aurea Technology
/// - Author		:	Matthieu Grovel
///
///	\section des_sec Description
///	This header document provide the prototypes and descriptions of all functions    
///	integrated on the dynamic library file (.dll).
/// Those functions allows to control Single Photon Detector device also named "SPD"
/// 
///	\section imp_sec Important
///
/// More or less functions are available according to the device type.	
///	The compatibility depends on the device part number recovered by
///	the "GetSystemVersion" function.								
///	Please see notes functions to check the compatibility with your device:  	
///   -> SPD version compatibility:  PN_SPD_x_Mx_xx_xx_xx 		
///
/// \section rev_sec Revisions
///
/// v1.0 (27/09/13)
/// - First release
/// 
/// v1.1 (17/01/14)
/// - Suppress initial functions:
///		- ArmEP()
///		- GetTDCdataEP()
///		- StartTDCcalibration()
///		- StopTDCcalibration()
/// - Add functions:
///		- TDCinitialization()
///		- GetNtdcHistogramData()
///		- SetOutputFormat()
///		- GetOutputFormat()
///		- SetIntegTime()
///		- GetIntegTime()
///		- SetAnalogOutGain()
///		- GetAnalogOutGain()
///		- SetDetOutDelay()
///		- GetDetOutDelay()
///
///	v1.2 (12/02/14)
/// - Suppress functions:
///		- GetTdcData()
/// - Add functions:
///		- GetTdcHistogramData()
///		- GetNtdcHistogramData()
/// 
/// v1.3 (04/03/14)
/// - Improvement functions:
///		- GetTdcHistogramData()
///		- GetNtdcHistogramData()
/// 
/// v1.4 (23/09/14)
/// - Improvement functions:
///		- GetNtdcHistogramData()
///		- GetsystemFeatures()
/// 
/// v1.5 (28/10/14)
/// - Suppress initial functions:
///		- GetCountingMode()
///		- SetCountingMode()
/// 
/// 1.6 (27/11/14)
/// - Add functions:
///		- StartELTDC()
///		- StopELTDC()
///		- GetELTDCdata()
///		- ListATdevices()
/// - Modification functions:
///		- GetLTDCdataCh1()
///		- GetLTDCdataCh2()
/// - Replacing functions:
///		- OpenSPDsystem()  -> OpenATdevice()
///		- CloseSPDsystem() -> CloseATdevice()
/// 
/// v1.7 (25/02/15)
/// - Internal features modifications
/// 
/// v1.8 (02/03/15)
/// - Improvement of ListATdevices()
/// 
/// v1.9 (21/08/15)
/// - Improvement of functions:
///		- GetTdcHistogramData()
///		- GetNtdcHistogramData()
/// 
/// v1.10 (21/10/15)
/// - Improvement of StartTDCmeasures() function
/// 
/// v1.11 (01/12/15)
/// - Minor internal modification
/// 
/// v1.12 (23/12/15)
/// - Improvement of all functions for triple and quad modules
/// - Modification of functions comments 
/// - Modification of functions returns
/// - Add functions:
///		- GetClkCountModule()
/// 
/// v1.13 (27/01/16)
/// - Replacing functions:
///		- GetCountingRate() -> GetIntegrationTime()
///		- SetCountingRate() -> SetIntegrationTime()
///		- GetIntegTime() -> GetIntegTimeAO()
///		- SetIntegTime() -> SetIntegTimeAO()
/// - Improvement of functions:
///		- GetCLKCountData()
///		- GetClkCountModule()
/// - Add some comments
/// - Change comments:
///		-invert mode value of GetVisibleModuleDetectionMode() and SetVisibleModuleDetectionMode()
/// 
/// v1.14 (23/11/16)
/// - Improvement of internal functions to recover clock & detection at speed up to 10ms
/// - Modification of GetIntegrationTime() and SetIntegrationTime() comments
/// 
/// v1.15 (16/12/16)
/// - Suppress of GetLTDCdataCh2() function
/// - Rename GetLTDCdataCh1() -> GetLTDCdata()
/// - Improvement of functions:
///		- StartLTDCch()
///		- StartELTDC()
///		- GetLTDCdata()
///		- GetELTDCdata()
/// 
/// v1.16 (12/01/17)
/// - Improvement of internals functions to close the software cleaner way 
/// 
/// v1.17 (10/08/17)
/// - Rename functions:
///		- SetVisibleModuleDetectionMode() to SetModuleDetectionMode() 
///		- GetVisibleModuleDetectionMode() to GetModuleDetectionMode()
/// 
/// v1.18 (17/08/17)
/// - Add function:
///		- StopATactivity()
/// - Improvement of the TDC calibration
/// - Add comments
/// 
/// v1.19 (11/09/17)
/// - Decimal separator fix '.' for all functions
/// - Improve GetNtdcHistogramData() function
/// 
/// v1.20 (29/05/17)
/// - Improvment of TDC functions to match new correlator
/// 
/// v1.21 (14/02/18)
/// - Improvement of SetDeadTime() function
/// 
/// v1.22 (05/02/19)
/// - Rename:
///		- systemFeatures() to GetSystemFeatures()
/// 
/// v1.23 (25/02/20)
/// - Fix bad USB frame recoverd after frequency settings
/// 
/// v2.00 (16/07/21)
/// - Add Device index in all functions
/// - Modify all get function to return value by pointer
/// - Remove correlator elements
/// - Add SPD_ at the beginning of all functions
/// - Replace functions:
/// 	- SPD_listATdevices() to SPD_listDevices()
/// 	- SPD_openATdevice() to SPD_openDevice()
/// 	- SPD_closeATdevice() to SPD_closeDevice()
/// 	- SPD_saveAll() to SPD_saveAllSettings()
/// 	- SPD_getSPDversio() to SPD_getSystemVersion()
////////////////////////////////////////////////////////////////////////////// 


#ifndef _SPD_H
#define _SPD_H

#ifndef DOXYGEN_SHOULD_SKIP_THIS

// Define LIB_CALL for any platform
#if defined _WIN32 || defined __CYGWIN__
#ifdef WIN_EXPORT
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllexport))
#else
#define LIB_CALL __declspec(dllexport) __cdecl
#endif
#else
#ifdef __GNUC__
#define LIB_CALL __attribute__ ((dllimport))
#else
#define LIB_CALL __declspec(dllimport) 
#endif
#endif
#else
#if __GNUC__ >= 4
#define LIB_CALL __attribute__ ((visibility ("default")))
#else
#define LIB_CALL
#endif
#endif

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers
#include <Windows.h>
#include "conio.h"
#define delay(x) Sleep(x)
#elif __unix
#include <cstring>
#include <unistd.h>
#define delay(x) usleep(x*1000)
#else
#include <unistd.h>
#define delay(x) usleep(x*1000)
#endif

#ifdef _WIN32 
#define secure_strtok strtok_s
#else
#define secure_strtok strtok_r
#endif	

#endif /* DOXYGEN_SHOULD_SKIP_THIS */

#if defined(__cplusplus)
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_getLibVersion(unsigned short *value)
///	\brief		Get the librarie version
/// \details	Return the version librarie in format 0x0000MMmm \n
///				with: MM=major version \n
///					  mm=minor version			  
///
/// \param		*value	return lib version by pointer \n
///						Format: 0xMMmm		\n
///						with:	MM: major version	\n
///								mm: minor version
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///
short LIB_CALL SPD_getLibVersion(unsigned short* value);

/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_listDevices(char** devices, short* number)
/// \brief		List SPD devices connected
/// \details	List and enumerate all SPD devices connected
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		**devices pointer to the table buffer which contain list of devices connected \n
///						 Output format: "deviceName - serialNumber" \n
///						 Example:									\n
///						 devices[0]="SPD - SN_xxxxx1xxxxx\r\n"		\n
///						 devices[1]="SPD - SN_xxxxx6xxxxx\r\n"		\n

/// \param		*number pointer to the number devices connected
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    
///		
///
short LIB_CALL SPD_listDevices(char** devices, short* number);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_openDevice(short iDev)
/// \brief		Open and initialize Aurea Technology device
/// \details	Open and initialize Aurea Technology device pointed by index parameter.
///	\note		Mandatory to do before any other action on the system device.
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error 
///		
///
short LIB_CALL SPD_openDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_stopActivity(short iDev)
///	\brief		Stop Aurea Technology device activity.
/// \details	Stop all the blocking activities running.
///	\note		Advisable to do before each end of device closing.
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_stopActivity(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_closeDevice(short iDev)
///	\brief		Close Aurea Technology device
/// \details	Close Aurea Technology Device previously opened. 
///	\note		Mandory to do after each end of system transfer.
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_closeDevice(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			SPD_getSystemFeature(short iDev, short feature, short* option) 
///	\brief		Get SPD feature
/// \details	Read EEPROM to recovery one feature of SPD
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		feature 0: module type			\n
///						1: module performance	\n
///						2: module number		\n	
/// 
/// \param		value	Pointer to the feature value
///
///	\return		-1 : Parameter error \n
///				0 : Setting failed   \n
///				1 : Setting applied
///
///
short LIB_CALL SPD_getSystemFeature(short iDev, short feature, short* option);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getSystemVersion(short iDev, char *version) 
/// \brief		Get SPD version
/// \details	Get SPD version: Serial number, product number and firmware version
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		version	Pointer to the buffer which receive the SPD version. \n
///						String format: SN_'serialNumber':PN_'ProductNumber':'FirmwareVersion'\n
///						The receive buffer size must be of 64 octets min.
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getSystemVersion(short iDev, char *version);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getEfficiency(short iDev, short APD, short *efficiency) 
/// \brief		Get efficiency
/// \details	Get actual APD efficiency value (in %).
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number) 
/// 
/// \param		*efficiency	pointer to the efficiency value (format: short)
///				
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_getEfficiency(short iDev, short APD, short *efficiency);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setEfficiency(short iDev, short APD, short efficiency)
///	\brief		Set efficiency
/// \details	Set APD effciency value (in %).
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD 0 to n (with n according to SPD module number)
///
///	\param		efficiency	Efficiency value in % and in multiple of 5 (format: decimal)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_setEfficiency(short iDev, short APD, short efficiency);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getGateWidth(short iDev, short APD, double *gateWidth) 
///	\brief		Get gate width
/// \details	Get actual APD gate width value (in ns).
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
/// \param		*gateWidth	pointer to the gate width value (format: double)
/// 
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_getGateWidth(short iDev, short APD, double *gateWidth);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setGateWidth(short iDev, short APD, double gateWidth)
///	\brief		Set gate width
/// \details	Set APD gate width value (in ns).
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		gateWidth Gate width value (in double) 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_setGateWidth(short iDev, short APD, double gateWidth);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getDelay(short iDev, short APD, double *delay)
///	\brief		Get delay
/// \details	Get actual APD delay value (in ns).
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
/// 
/// \param		*delay	pointer to the delay value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_getDelay(short iDev, short APD, double *delay);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setDelay(short iDev, short APD, double delay)
///	\brief		Set delay
/// \details	Set APD dalay value (in ns).
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		delay	Delay value in ns (format: double) 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_setDelay(short iDev, short APD, double delay);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getDeadTime(short iDev, short APD, double *deadTime)
///	\brief		Get deadtime
/// \details	Get actual APD deadtime value (in us).
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
/// 
/// \param		*deadTime	pointer to the deadtime value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_getDeadTime(short iDev, short APD, double *deadTime);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setDeadTime(short iDev, short APD, double deadTime)
///	\brief		Set deadtime
/// \details	Set APD deadtime value (in us).
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		deadTime DeadTime value in us (format: double) 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_setDeadTime(short iDev, short APD, double deadTime);


////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getClockingMode(short iDev, short *mode)
///	\brief		Get clocking mode
/// \details	Get actual clocking mode (External or Internal).
///	\note		SPD version compatibility: all
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*mode	pointer to the mode (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_getClockingMode(short iDev, short *mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setClockingMode(short iDev, short mode)
///	\brief		Set clocking mode
/// \details	Set clocking mode (External or Internal).
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		mode 0 : External clock \n
///					 1 : Internal clock
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_setClockingMode(short iDev, short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getClockFreqRange(short iDev, char *FreqRange)
///	\brief		Get clock frequency range
/// \details	Get clock frequency range and number of available frequency with the SPD device.
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		FreqRange	Pointer to the buffer which to receive the frequency range.		\n
///							String format: numberOfAvailableFreq;Frequency1;Frequency2;...; \n
///							Warning: 'numberOfAvailableFreq' is in decimal. \n
///							The receive buffer size must be of 256 octets min. 
///		
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getClockFreqRange(short iDev, char *FreqRange);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setClockFreq(short iDev, short indexFreq)
/// \brief		Set clock frequency
/// \details	Set clock frequency.
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		indexFreq	Index of the desired frequency (format: decimal) 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_setClockFreq(short iDev, short indexFreq);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getClockFreq(short iDev, short *indexFreq)
/// \brief		Get clock frequency
/// \details	Get actual clock frequency.
///	\note		SPD version compatibility: all
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*indexFreq	pointer to the frequency index (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
///
short LIB_CALL SPD_getClockFreq(short iDev, short *indexFreq);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getCLKCountData(short iDev, unsigned long *CLK1,unsigned long *CLK2,
///									  unsigned long *Count1,unsigned long *Count2)
///	\brief		Get clock and count data
/// \details	Get both clock value and photons count. \n
///				Blocking function(timeout of 11s).
///	\note		SPD version compatibility: all
///	
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		CLK1	Pointer to the APD1 clock value (format: decimal)	
/// 
/// \param		CLK2	Pointer to the APD2 clock value (format: decimal)
/// 
/// \param		Count1	Pointer to the APD1 count value (format: decimal)
/// 
/// \param		Count2	Pointer to the APD2 count value (format: decimal)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getCLKCountData(short iDev, unsigned long *CLK1,unsigned long *CLK2,unsigned long *Count1,unsigned long *Count2);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getClkCountModule(short iDev, short module, unsigned long *Clock,unsigned long *Count)
///	\brief		Get clock and count data of specific module
/// \details	Get both clock value and photons count for the module selected. \n
///				Blocking function(timeout of 11s).
///	\note		SPD version compatibility: all
///	
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		module	Index of the module (0 (Module 1) to n, according the number of SPD module)
/// 
/// \param		Clock	Pointer to the module clock value (format: decimal)	
/// 
/// \param		Count	Pointer to the module count value (format: decimal)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getClkCountModule(short iDev, short module, unsigned long *Clock,unsigned long *Count);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_saveAllSettings(short iDev)
///	\brief		Save all parameters
/// \details	Save all parameters (gate width, delay, deadtime, ...).
///	\note		SPD version compatibility: all
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_saveAllSettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_factorySettings(short iDev)
///	\brief		SPD factory settings
/// \details	Set all parameters with factory settings.
///	\note		SPD version compatibility: all
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_factorySettings(short iDev);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_setIntegrationTime(short iDev, double time)
/// \brief		Set the integration time of photons.
/// \details	Set the integration time of photons counted before sending the result.  
///				Time value between 0.1s to 10s.
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		time	Time value in s (format: double) \n 
///						Value between 0.01 to 10.0s with step of 0.01s
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_setIntegrationTime(short iDev, double time);


/////////////////////////////////////////////////////////////////////////////////////
/// \fn			short SPD_getIntegrationTime(short iDev, double *time) 
/// \brief		Get integration time value.
/// \details	Get the integration time of photons counted. Time value between 0.01s 
///				to 10s. 
///	\note		SPD version compatibility: all
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*time	pointer to the time value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///		
///
short LIB_CALL SPD_getIntegrationTime(short iDev, double *time);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getEfficiencyRange(short iDev, short APD, char *range)
///	\brief		Get efficiency range
///	\details	Send all efficiency available value for APD selected
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
/// 
/// \param		range	Pointer to the string buffer of the efficiency range. \n
///						String format: "val1%val2%... " \n
///						'val1' represent the efficiency value with 4 characters \n 
///						followed of the '%' character.
///						The receive buffer size must be of 64 octets min. 
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getEfficiencyRange(short iDev, short APD, char *range);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getDelayRange(short iDev, short APD, double *MinVal, double *MaxVal)
///	\brief		Get delay range
///	\details	Get min and max values of the delay range
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		MinVal	pointer to the current delay min value (format: double) 
///
///	\param		MaxVal	pointer to the current dealy max value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getDelayRange(short iDev, short APD, double *MinVal, double *MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getDeadTimeRange(short iDev, short APD, double *MinVal, double *MaxVal)
///	\brief		Get deadtime range
///	\details	Get min and max values of the deadtime range
///	\note		SPD version compatibility: PN_SPD_A_Mx_xx_xx
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		MinVal	pointer to the current deadtime min value (format: double) 
///
///	\param		MaxVal	pointer to the current deadtime max value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getDeadTimeRange(short iDev, short APD, double *MinVal, double *MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getGateWidthRange(short iDev, short APD, double *MinVal, double *MaxVal)
///	\brief		Get deadtime range
///	\details	Get min and max values of the gate width range
///	\note		SPD version compatibility: all
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		APD	0 to n (with n according to SPD module number)
///
///	\param		MinVal	pointer to the current gate width min value (format: double) 
///
///	\param		MaxVal	pointer to the current gate width max value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getGateWidthRange(short iDev, short APD, double *MinVal, double *MaxVal);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setModuleDetectionMode(short iDev, short module, short mode)
///	\brief		Set module detection mode 
///	\details	Set the type of detection mode (gated or continuous)
///	\note		SPD version compatibility: PN_SPD_V_Mx_xx_xx and PN_SPD_x_Mx_xx_HM
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		module	0 to n (with n according to SPD module number)
///
/// \param		mode	0: continuous	\n
///						1: gated
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_setModuleDetectionMode(short iDev, short module, short mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getModuleDetectionMode(short iDev, short module, short *mode)
///	\brief		Get module detection mode 
///	\details	Get the type of the detection mode (gated or continuous)
///	\note		SPD version compatibility: PN_SPD_V_Mx_xx_xx and PN_SPD_x_Mx_xx_HM
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		module	0 to n (with n according to SPD module number)
/// 
/// \param		*mode	pointer to the mode (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getModuleDetectionMode(short iDev, short module, short *mode);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setOutputFormat(short iDev, short format)
///	\brief		Set output detection format
///	\details	Set output detection format in numerical or analogic
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		format	0: numerical \n
///						1: analogic
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_setOutputFormat(short iDev, short format);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getOutputFormat(short iDev, short *format)
///	\brief		Get output detection format 
///	\details	
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*format	pointer to the format (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getOutputFormat(short iDev, short *format);



/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setIntegTimeAO(short iDev, double time)
///	\brief		Set integration time 
///	\details	Set integration time of detection counted, for analog output
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		time	integration time of 0.1 to 100 in ms
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_setIntegTimeAO(short iDev, double time);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getIntegTimeAO(short iDev, double *time)
///	\brief		Get integration time 
///	\details	Get integration time for analog output
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*time	pointer to the time value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getIntegTimeAO(short iDev, double *time);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setAnalogOutGain(short iDev, short gain)
///	\brief		Set analog gain  
///	\details	Set gain for analogic detection output
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		gain	Analogic gain of 1 to 100
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_setAnalogOutGain(short iDev, short gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getAnalogOutGain(short iDev, short *gain)
///	\brief		Get analog gain
///	\details	Get gain of analogic detection output
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_AO
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*gain	pointer to the gain value (format: short)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getAnalogOutGain(short iDev, short *gain);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_setDetOutDelay(short iDev, double delay)
///	\brief		Set detection out delay   
///	\details	Set additionnal delay on detection OUT
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_OD
/// 
/// \param		iDev	Device index indicate by "SPD_listDevices" function
///
/// \param		delay	Adjustable delay: 0 to 127.5 in ns
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_setDetOutDelay(short iDev, double delay);


/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getDetOutDelay(short iDev, double *delay)
///	\brief		Get detection out delay
///	\details	Get additionnal delay applied on detection OUT 
///	\note		SPD version compatibility: PN_SPD_x_Mx_xx_xx_OD
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
/// \param		*delay	pointer to the delay value (format: double)
///
/// \return		
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getDetOutDelay(short iDev, double *delay);

/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getSystemAlarms(char *alarm)
///	\brief		Get System Alarms
///	\details	Get active alarm detected by system
///
/// \param		iDev	Device index indicate by "SPD_listDevices" function
/// 
///	\param		alarm	Pointer to the buffer which receive the alarm description. \n
///						The receive buffer size must be of 64 octets min.
///
/// \return
///				1 : Alarm detected	   \n
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getSystemAlarms(short iDev, char* alarm);

/////////////////////////////////////////////////////////////////////////////////////
///	\fn			short SPD_getSystemHardwareVersion(short iDev, short card, unsigned short* version, unsigned short* model)
///	\brief		Get SPD hardware card version & model
///	\details	
///
/// \param		iDev	Device index indicate by "CPC_listDevices" function
/// \param		card	0 : MCE		\n
///						1 : MGI		\n
///						2 : MTCSPC	\n
///						3 : MTM		\n
///						4 : MCT		
/// \param		*version    return the card version by pointer
/// \param      *model      return the card model by pointer
///
/// \return
///				0 : Function success   \n
///			   -1 : Function failed    \n
///			   -2 : Parameter(s) error \n
///			   -4 : iDev index Out Of Range
///
///
short LIB_CALL SPD_getSystemHardwareVersion(short iDev, short card, unsigned short* version, unsigned short* model);

#if defined(__cplusplus)
}
#endif

#endif